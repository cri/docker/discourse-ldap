#!/bin/bash

export PATH="$PATH:/opt/venv/bin:$PATH"

cd /app/discourse-ldap

echo "Running at $(date)"

exec "$@"

