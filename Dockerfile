FROM registry.cri.epita.fr/cri/docker/mirror/python:3.10-slim

ENV DEBIAN_FRONTEND=noninteractive \
    LC_ALL=C.UTF-8 \
    LANG=C.UTF-8 \
    PYTHONDONTWRITEBYTECODE=1 \
    POETRY_VIRTUALENVS_CREATE=false \
    VENV_PATH="/opt/venv" \
    PATH="/opt/venv/bin:$PATH"

RUN apt-get update && \
    apt-get install -y --no-install-recommends build-essential python3-dev \
    libldap2-dev libsasl2-dev slapd ldap-utils tox lcov valgrind

ARG WORKDIR=/app/discourse-ldap
ARG USER_UID=1000
ARG USER_GID=1000

RUN mkdir -p "$WORKDIR"
RUN useradd -r -u "$USER_UID" -d "$WORKDIR" discourse-ldap

WORKDIR "$WORKDIR"

COPY pyproject.toml poetry.lock ./

RUN pip install poetry && \
    poetry install --no-dev

COPY . .

RUN chown -R discourse-ldap:"$USER_GID" "$WORKDIR"
USER discourse-ldap:"$USER_GID"

ENTRYPOINT ["/app/discourse-ldap/entrypoint.sh"]

CMD ["python", "/app/discourse-ldap/src/main.py"]
