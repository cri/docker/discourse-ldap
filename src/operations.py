class GroupOperation:
    def __init__(self, ldap_group, ldap_users, discourse):
        self.group_name = ldap_group["cn"]
        self.add_group = False
        self.users_to_add = []
        self.users_to_remove = []

        self._calculate_group_operations(ldap_group, ldap_users, discourse)

    def _calculate_group_operations(self, ldap_group, ldap_users, discourse):
        discourse_group_members = []
        if discourse.group_exists(ldap_group["cn"]):
            users = discourse.get_users_in_group(ldap_group["cn"])
            for user in users:
                discourse_group_members.append(user["username"])
        else:
            self.add_group = True

        ldap_group_members = []
        for user in ldap_group["members"]:
            if user not in ldap_users:
                continue
            ldap_user = ldap_users[user]
            username = ldap_user["uid"]

            if not discourse.user_exists(username):
                continue

            ldap_group_members.append(username)
            if username in discourse_group_members:
                continue

            self.users_to_add.append(username)

        for user in discourse_group_members:
            if user not in ldap_group_members:
                self.users_to_remove.append(user)

    def __str__(self):
        output = f"Group operations on '{self.group_name}':\n"
        output += f"\tAdd group: {self.add_group}\n"
        output += f"\tUsers to add: {len(self.users_to_add)}\n"
        output += f"\tUsers to remove: {len(self.users_to_remove)}\n"
        return output

    def req_count(self):
        count = 1 if self.add_group else 0
        count += 1 if self.users_to_add else 0
        count += 1 if self.users_to_remove else 0
        return count

    def to_json(self):
        return {
            "group_name": self.group_name,
            "add_group": self.add_group,
            "users_to_add": self.users_to_add,
            "users_to_remove": self.users_to_remove,
        }
