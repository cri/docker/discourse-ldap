import discourse
import forge_ldap
import operations

import json
import os
import threading

LDAP_SERVER = os.environ["LDAP_SERVER"]
LDAP_PORT = os.environ["LDAP_PORT"]
LDAP_USER = os.environ["LDAP_USER"] if "LDAP_USER" in os.environ else ""
LDAP_PASSWORD = os.environ["LDAP_PASSWORD"] if "LDAP_PASSWORD" in os.environ else ""
LDAP_GROUP_BASE_DN = os.environ["LDAP_GROUP_BASE_DN"]
LDAP_USER_BASE_DN = os.environ["LDAP_USER_BASE_DN"]

DISCOURSE_PROTO = os.environ["DISCOURSE_PROTO"]
DISCOURSE_HOST = os.environ["DISCOURSE_HOST"]
DISCOURSE_API_KEY = os.environ["DISCOURSE_API_KEY"]
DISCOURSE_API_USERNAME = os.environ["DISCOURSE_API_USERNAME"]

news = discourse.Discourse(
    DISCOURSE_PROTO, DISCOURSE_HOST, DISCOURSE_API_KEY, DISCOURSE_API_USERNAME
)

DRY_RUN_ENV = os.environ.get("DRY_RUN", "false")
DRY_RUN = DRY_RUN_ENV.lower() in ["true", "1", "t", "y", "yes"]

# Check if arg --dry-run is present
if not DRY_RUN and "--dry-run" in os.sys.argv:
    DRY_RUN = True

if DRY_RUN:
    print("[SCRIPT] Running in dry-run mode")

with forge_ldap.ForgeLdap(
    LDAP_SERVER,
    LDAP_PORT,
    LDAP_USER,
    LDAP_PASSWORD,
    LDAP_GROUP_BASE_DN,
    LDAP_USER_BASE_DN,
) as ldap:
    groups = ldap.get_groups()
    users = ldap.get_users()

    print("[SCRIPT] Parsed {} users".format(len(users)))
    print("[SCRIPT] Parsed {} groups".format(len(groups)))

    # split groups in four lists (5 if len(groups) is not divisible by 4)
    split_groups = []
    for i in range(4):
        split_groups.append(groups[i * len(groups) // 4 : (i + 1) * len(groups) // 4])

    # add the remaining groups to the last thread
    if len(groups) % 4 != 0:
        split_groups.append(groups[4 * len(groups) // 4 :])

    actions = {}

    def thread_function(groups):
        for group in groups:
            action = operations.GroupOperation(group, users, news)
            if action.req_count() == 0:
                continue
            actions[group["cn"]] = action

    threads = []
    for groups in split_groups:
        x = threading.Thread(target=thread_function, args=(groups,))
        threads.append(x)
        x.start()

    for thread in threads:
        thread.join()

    sum_actions = sum(action.req_count() for action in actions.values())

    print("[SCRIPT] Total actions needed: {}".format(sum_actions))

    # split actions in four lists (5 if sum_actions is not divisible by 4)
    split_actions = []
    actions_list = list(actions.values())

    count_per_thread = sum_actions // 4
    for i in range(4):
        split_actions.append(
            actions_list[i * count_per_thread : (i + 1) * count_per_thread]
        )

    # add the remaining actions to the last thread
    if sum_actions % 4 != 0:
        split_actions.append(actions_list[4 * count_per_thread :])

    def thread_function(actions):
        for action in actions:
            group_name = action.group_name

            if action.add_group:
                if DRY_RUN:
                    print(
                        "[SCRIPT] Would add group {} with {} users".format(
                            group_name, len(action.users_to_add)
                        )
                    )
                else:
                    news.create_group(group_name)

            if action.users_to_add:
                if DRY_RUN:
                    print(
                        "[SCRIPT] Would add {} users to group {}".format(
                            len(action.users_to_add), group_name
                        )
                    )
                else:
                    news.add_users_to_group(group_name, action.users_to_add)

            if action.users_to_remove:
                if DRY_RUN:
                    print(
                        "[SCRIPT] Would remove {} users from group {}".format(
                            len(action.users_to_remove), group_name
                        )
                    )
                else:
                    news.remove_users_from_group(group_name, action.users_to_remove)

            if action.add_group:
                print(
                    "[SCRIPT] Added group {} with {} users".format(
                        group_name, len(action.users_to_add)
                    )
                )
            else:
                print(
                    "[SCRIPT] Updated group {}, added {} users, removed {} users".format(
                        group_name,
                        len(action.users_to_add),
                        len(action.users_to_remove),
                    )
                )

    threads = []
    for actions in split_actions:
        x = threading.Thread(target=thread_function, args=(actions,))
        threads.append(x)
        x.start()

    if DRY_RUN:
        # compute all operations and output them to a file
        operations = {}
        for action in actions_list:
            operations[action.group_name] = {
                "add_group": action.add_group,
                "users_to_add": action.users_to_add,
                "users_to_remove": action.users_to_remove,
            }
        with open("operations.json", "w") as f:
            json.dump(operations, f, indent=4)

    for thread in threads:
        thread.join()

    print("[SCRIPT] Done, edited {} groups".format(len(actions_list)))
