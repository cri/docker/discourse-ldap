from ldap import initialize, SCOPE_SUBTREE, SCOPE_ONELEVEL


class ForgeLdap:
    def __init__(
        self,
        ldap_server,
        ldap_port,
        ldap_user,
        ldap_password,
        ldap_group_base_dn,
        ldap_user_base_dn,
    ):
        self.ldap_server = ldap_server
        self.ldap_port = ldap_port
        self.ldap_user = ldap_user
        self.ldap_password = ldap_password
        self.ldap_group_base_dn = ldap_group_base_dn
        self.ldap_user_base_dn = ldap_user_base_dn

    def connect(self):
        try:
            self.ldap_conn = initialize(self.ldap_server + ":" + self.ldap_port)
            if self.ldap_user != "" and self.ldap_password != "":
                self.ldap_conn.simple_bind_s(self.ldap_user, self.ldap_password)
        except Exception as e:
            print(e)

    def close(self):
        self.ldap_conn.unbind()

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def search_groups(self, search_filter, search_scope=None):
        search_scope = SCOPE_SUBTREE if search_scope is None else search_scope
        result = self.ldap_conn.search_s(
            self.ldap_group_base_dn, search_scope, search_filter
        )
        return result

    def search_users(self, search_filter, search_scope=None):
        search_scope = SCOPE_SUBTREE if search_scope is None else search_scope
        result = self.ldap_conn.search_s(
            self.ldap_user_base_dn, search_scope, search_filter
        )
        return result

    def get_groups(self):
        search_filter = "(objectClass=groupOfMembers)"
        result = self.search_groups(search_filter, SCOPE_ONELEVEL)

        groups = []
        for group in result:
            (dn, group_info) = group
            if "cn" not in group_info:
                continue
            data = {
                "cn": group_info["cn"][0].decode("utf-8"),
                "members": [],
            }
            if "member" in group_info:
                for member in group_info["member"]:
                    data["members"].append(member.decode("utf-8"))
            groups.append(data)

        return groups

    def get_users(self):
        # This search filter select all accounts
        # Filters out those without uid or mail
        # Filters out those with the following gidNumber
        #  - 150: service accounts
        #  - 5000: invités
        #  - 5001: chats
        search_filter = "(&(objectClass=posixAccount)(&(uid=*)(mail=*)(!(|(gidNumber=150)(gidNumber=5000)(gidNumber=5001)))))"
        result = self.search_users(search_filter, SCOPE_ONELEVEL)

        users = {}
        for user in result:
            (dn, user_info) = user
            if "cn" not in user_info:
                continue
            data = {
                "dn": dn,
                "uid": user_info["uid"][0].decode("utf-8"),
                "mail": user_info["mail"][0].decode("utf-8"),
                "fullname": user_info["cn"][0].decode("utf-8"),
            }
            users[dn] = data

        return users

    def get_group_members(self, group_name):
        search_filter = "(cn=" + group_name + ")"
        result = self.search_groups(search_filter, SCOPE_SUBTREE)

        members = []
        for group in result:
            (dn, group_info) = group
            if "member" not in group_info:
                continue
            for member in group_info["member"]:
                members.append(member.decode("utf-8"))

        return members


if __name__ == "__main__":
    ldap_server = "ldap.pie.cri.epita.fr"
    ldap_port = "389"
    ldap_user = ""
    ldap_password = ""
    ldap_group_base_dn = "ou=groups,dc=cri,dc=epita,dc=fr"
    ldap_user_base_dn = "ou=users,dc=cri,dc=epita,dc=fr"

    with ForgeLdap(
        ldap_server,
        ldap_port,
        ldap_user,
        ldap_password,
        ldap_group_base_dn,
        ldap_user_base_dn,
    ) as ldap:
        result = ldap.get_groups()
        # print(result)
        result = ldap.get_users()
        # print(result)
        result = ldap.get_group_members("administratives")
        print(result)
