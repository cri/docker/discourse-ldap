import hashlib
import random
import requests
from json import dumps
from time import sleep

GROUP_PREFIX = "ldap_"
COUNT = 0


class Discourse:
    def __init__(self, proto, host, api_key, api_username):
        self.url = "{0}://{1}".format(proto, host)
        self.api_key = api_key
        self.api_username = api_username
        self.all_groups = self.get_all_groups()
        self.all_users = self.get_all_users()

    def _get_headers(self, content_type="application/json"):
        return {
            "Content-Type": content_type,
            "Api-Key": self.api_key,
            "Api-Username": self.api_username,
        }

    def _make_request(self, method, endpoint, data=None, json=None):
        global COUNT
        COUNT += 1
        print("[WEB] Sending request : {}".format(endpoint))
        response = method(
            self.url + endpoint, headers=self._get_headers(), data=data, json=json
        )
        code = response.status_code
        if code == 429:
            if "Retry-After" in response.headers:
                time_str = response.headers["Retry-After"]
                if time_str.isdigit():
                    time = int(time_str)
                    print(
                        "[WEB] Rate limit exceeded, sleeping for {0} seconds".format(
                            time
                        )
                    )
                    sleep(time)
                    return self._make_request(method, endpoint, data, json)

            print("[WEB] Rate limit error, see below for details")
            print("Endpoint: {}".format(endpoint))
            print("Method: {}".format(method))
            data = {"headers": dict(response.headers), "response": response.text}
            print(dumps(data, indent=4))
            raise Exception("Rate limit exceeded for request {0}".format(COUNT))
        elif code == 400 or code == 422:
            print("[WEB] Bad request, see below for details")
            print("Endpoint: {}".format(endpoint))
            print("Method: {}".format(method))
            sent_data = data if data else dumps(json, indent=4)
            print("Request data: {}".format(sent_data))
            data = {"headers": dict(response.headers), "response": response.text}
            if not response.text:
                data["response"] = "No response"
            print(dumps(data, indent=4))
            return code, response
        return code, response

    def get_all_groups(self, page=0, groups=None):
        if groups is None:
            groups = {}

        endpoint = "/groups.json?page={0}".format(page)
        code, data = self._make_request(requests.get, endpoint)
        if code != 200:
            raise Exception("Unexpected response code: {0}".format(code))

        data = data.json()
        new_groups = 0
        for group in data["groups"]:
            new_groups += 1
            groups[group["name"]] = group["id"]

        if new_groups == 0:
            return groups

        return self.get_all_groups(page + 1, groups)

    def _get_all_users_flag(self, flag, page=0):
        endpoint = "/admin/users/list/{0}.json?page={1}".format(flag, page)
        code, data = self._make_request(requests.get, endpoint)
        if code != 200:
            raise Exception("Unexpected response code: {0}".format(code))

        data = data.json()
        if len(data) == 0:
            return []

        return [user["username"] for user in data] + self._get_all_users_flag(
            flag, page + 1
        )

    def get_all_users(self, page=0):
        flags = ["active", "new", "staff", "suspended", "blocked", "suspect"]
        users = []

        for flag in flags:
            users += self._get_all_users_flag(flag)

        return users

    def user_exists(self, username):
        return username in self.all_users

    def group_exists(self, group_name):
        group_name = GROUP_PREFIX + group_name
        return group_name in self.all_groups

    def create_group(self, group_name):
        group_name = GROUP_PREFIX + group_name
        endpoint = "/admin/groups"

        data = {
            "name": group_name,
            "public_admission": False,
            "public_exit": False,
        }

        code, resp = self._make_request(requests.post, endpoint, json=data)

        if code != 200:
            raise Exception("Unexpected response code: {0}".format(code))

        self.all_groups[group_name] = resp.json()["basic_group"]["id"]

    def create_user(self, username, email, name):
        endpoint = "/users.json"
        # generate a random password with 64 characters
        number = random.randint(0, 1000000000)
        hashed = hashlib.sha256(str(number).encode("utf-8")).hexdigest()[0:48]
        data = {
            "name": name,
            "username": username,
            "email": email,
            "password": hashed,
            "active": True,
        }
        code, data = self._make_request(requests.post, endpoint, json=data)
        if code != 200:
            print("Error creating user: {}".format(username))
            print(data.headers)
            print(data.text)
            raise Exception("Unexpected response code: {0}".format(code))

    def get_users_in_group(self, group_name, offset=0):
        raw_group_name = group_name
        group_name = GROUP_PREFIX + group_name
        endpoint = "/groups/{0}/members.json?limit=500&offset={1}".format(
            group_name, offset
        )
        code, resp = self._make_request(requests.get, endpoint)
        if code != 200:
            print("Error getting users in group: {}".format(group_name))
            print("Endpoint: {}".format(endpoint))
            raise Exception("Unexpected response code: {0}".format(code))

        jsondata = resp.json()
        members = jsondata["members"]

        if "meta" in jsondata and "total" in jsondata["meta"]:
            total = jsondata["meta"]["total"]
            if total > offset + 500:
                members += self.get_users_in_group(raw_group_name, offset + 500)

        return members

    def add_users_to_group(self, group_name, usernames):
        if not group_name.startswith(GROUP_PREFIX):
            group_name = GROUP_PREFIX + group_name

        if group_name not in self.all_groups:
            self.create_group(group_name)

        group_id = self.all_groups[group_name]
        endpoint = "/groups/{0}/members.json".format(group_id)

        to_add = []
        if len(usernames) > 950:
            to_add = usernames[:950]
            usernames = usernames[950:]
        else:
            to_add = usernames
            usernames = []

        if len(to_add) == 0:
            return

        data = {"usernames": ",".join(to_add)}
        code, data = self._make_request(requests.put, endpoint, json=data)
        if code != 200 and code != 400 and code != 422:
            raise Exception("Unexpected response code: {0}".format(code))
        if usernames:
            self.add_users_to_group(group_name, usernames)

    def remove_users_from_group(self, group_name, usernames):
        if not group_name.startswith(GROUP_PREFIX):
            group_name = GROUP_PREFIX + group_name

        group_id = self.all_groups[group_name]
        endpoint = "/groups/{0}/members.json".format(group_id)

        to_del = []
        if len(usernames) > 950:
            to_del = usernames[:950]
            usernames = usernames[950:]
        else:
            to_del = usernames
            usernames = []

        if len(to_del) == 0:
            return

        data = {"usernames": ",".join(to_del)}
        code, data = self._make_request(requests.delete, endpoint, json=data)
        if code != 200 and code != 400 and code != 422:
            raise Exception("Unexpected response code: {0}".format(code))

        if usernames:
            self.remove_users_from_group(group_name, usernames)
